Lien document explicatif :
https://drive.google.com/folderview?id=0ByHHhIH89vwpfjdMWHBqQU5NdmVaYV94VEt2QnBOYjNOcUxEaEVSbXRGcjFtdmpsRElhaUE&usp=sharing

Configuration de Git :
Placer vous dans le répertoire de projet où vous souhaitez cloner le dépôt local et taper les lignes de commandes suivantes :

git config --global color.diff auto
git config --global color.status auto
git config --global color.branch auto

git config --global user.name "votre_pseudo"
git config --global user.email moi@email.com

git init
git clone https://Warlot-PQ@bitbucket.org/firecrushers/php_project_lsi2.git

Comment ajouter votre travail :
git pull (avant de commencer à travailler pour que tout soit à jour)
git add <file>
git commit -am 'mon message lié au commit'
git push (envoie des données au serveur)

Supprimer de notre repository local toutes les branches remote qui n'existent plus : git remote prune origin