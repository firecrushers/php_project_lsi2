//===== redirect to planning view
var planningBtnElt = document.getElementById("planningBtn");
if (planningBtnElt != null) {
	planningBtnElt.addEventListener('click', handlePlanningView, false);
}
function handlePlanningView(e) {
	location.assign("../../application/controllers/planning.php");
}

//===== handle connexion / deconnexion
var connexionBtnElt = document.getElementById("connexionBtn");
if (connexionBtnElt != null) {
	connexionBtnElt.addEventListener('click', handleConnexion, false);
}
var deconnexionBtnElt = document.getElementById("deconnexionBtn");
if (deconnexionBtnElt != null) {
	deconnexionBtnElt.addEventListener('click', handleDeconnexion, false);
}
function handleConnexion(e) {
	location.assign("../../application/controllers/authent.php");
}
function handleDeconnexion(e) {
	//Deco in ajax rnequest and refresh page
	$.ajax({
		url : "../../application/controllers/ajax/deco.php",
		type : 'POST',
		data : '',
		dataType : 'json',
		success : function(JsonResponse, statut){ 
			if (JsonResponse.state == "Success"){
				location.reload();
			}
		}
	});
}

//=====  redirect to charge teacher view
var chargeTeachBtnElt = document.getElementById("chargeTeachBtn");
if (chargeTeachBtnElt != null) {
	chargeTeachBtnElt.addEventListener('click', handleChargeTeach, false);
}
function handleChargeTeach(e) {
	location.assign("../../application/controllers/chargeTeacher.php");
}

//===== redirect to profil view
var profilBtnElt = document.getElementById("profilBtn");
if (profilBtnElt != null) {
	profilBtnElt.addEventListener('click', handleProfilView, false);
}
function handleProfilView(e) {
	location.assign("../../application/controllers/profil.php");
}












var editModuleBtnElt = document.getElementById("editModuleBtn");
if (editModuleBtnElt != null) {
	editModuleBtnElt.addEventListener('click', handleEditModule, false);
}

function handleEditModule(e) {
	//AJAX TODO + refresh

}