<?php
Session_start();
$absolutePath = 'http://' .$_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']);
define('WEBROOT', substr($absolutePath, 0, strrpos($absolutePath, '/application')) .'/');

$login = '';
if (!empty($_SESSION['login'])) {
	$login = $_SESSION['login'];
}
define('LOGIN', $login);

require_once __DIR__ .'/models/Model.php';

class Controller {
	var $vars = array();
	var $layout = 'default';
	var $models = array('DBplannings');

	function __construct() {
		if (count(($this->models)) != 0) {
			foreach ($this->models as $v) {
			 $this->loadModel($v);
			}
		}
	}
	
	function setLayout($myLayout = 'default') {
		$this->layout = $myLayout;
	}

	function set($d = array()){
		$this->vars = array_merge($this->vars, $d);
	}
	
	function render($titlePage = 'Inconnue', $navTitle = '', $fileMame) {
		$DDE = $this->DBplannings->isDDE(LOGIN);
		$Responsable = $this->DBplannings->isResponsible(LOGIN);

		$pathLibrary = WEBROOT .'library';
		$pathController = WEBROOT .'application/controllers';
		$pathPublic = WEBROOT .'public';
		
		extract($this->vars);

		ob_start();
		require_once __DIR__ .'/views/'. $fileMame .'.php';
		$contentForLayout = ob_get_clean();
		
		if ($this->layout == 'false') {
			echo $contentForLayout;
		} else {
			require_once __DIR__  .'/views/layout/'. $this->layout .'.php';
		}
	}
	
	function loadModel($name) {
		require_once __DIR__ .'/models/'. $name .'.php';
		$this->$name = new $name();
	}
}
?>