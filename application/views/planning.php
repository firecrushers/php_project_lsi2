<?php 
if (@get_called_class() != 'Controller') {
	die('Access denied!');
}

$id_vcal = 'format_vcal';
$id_ical = 'format_ical';
$id_csv = 'format_csv';

/*
*	Space not allowed in id, so this function replace ' ' by '___'
*/
function trim_string($string = ''){
	return str_replace(' ', '___', $string);
}
//Beginning of the view
	if ($modification == true){
?>
<form class="navbar-form navbar-left">
	<button id="commitBtn" type="button" name="commit" class="btn btn-success">
		commit/update
	</button>
</form>
<?php
	}
?>
<ul class="nav navbar-nav navbar-right">
	<li class="dropdown">
      <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Exporter<span class="caret"></span></a>
      <ul class="dropdown-menu" role="menu">
        <li><a class='exportBtn' id='<?php echo $id_vcal; ?>' href="javascript:void(0);">format vCal</a></li>
        <li><a class='exportBtn' id='<?php echo $id_ical; ?>' href="javascript:void(0);">format iCal</a></li>
        <li><a class='exportBtn' id='<?php echo $id_csv; ?>' href="javascript:void(0);">format CSV</a></li>
      </ul>
    </li>
</ul>
<form class="navbar-form navbar-right">
	<select id='filiereSelecteur' class="selectpicker">
	<?php
		foreach ($publicList as $valueDisplayedName) {
	?>
			<option value='<?php echo $valueDisplayedName; ?>' <?php if ($valueDisplayedName == $publicSelected) {echo ' selected';} ?>><?php echo $valueDisplayedName; ?></option>
	<?php
		}
	?>
	</select>
	<input id="publicSelected" type="hidden" value="<?php echo $publicSelected; ?>">
	<button id="filiereBtn" type="button" name="filiere" class="btn btn-default ">
		Afficher
	</button>
</form>
<?php
	include __DIR__ .'/layout/Component/userMenuEnd.php';
?>
<div class="container-fluid">
	<div class="row">     
		<div class="table-responsive">
			<table class="table table-striped">
				<thead>
					<tr>               
					<th>Module</th>
					<th>Intervenant</th>
					<th>Nature</th>
					<?php
						if ($modification == true){
					?>
					<th>Quantite</th>
					<?php
						}
					?>
					<?php
						foreach ($weekNumbers as $keyWeekNumber => $keyWeekContent) {
							echo '<th>'. $keyWeekNumber .'<span class="hoursLeft">&nbsp;+<span id="headerweek'. $keyWeekNumber .'">'. ($keyWeekContent['nbHeuresMax'] - $keyWeekContent['heuresAffectees']) .'</span></span></th>';	
						}
					?>
					</tr>
				</thead>

				<tbody>
					<?php
						//Number of rows per module
						$coursesNumber = array();
						//Boolean to check if Intervenant or Nature is at the beginning of the row (if so then new row is requiered)
						$firstIntervenant = true;
						$firstNature = true;
						
						foreach ($schedule as $keyModule => $valueArrayIntervenant) {
							//Module
							$firstIntervenant = true;
							$firstNature = true;
					?>
							<tr>
					<?php
							//Update the number of rows per module
							foreach ($valueArrayIntervenant as $valueCourseNature) {
								if (!isset($coursesNumber[$keyModule])) {
									$coursesNumber[$keyModule] = 0;
								}
								$coursesNumber[$keyModule] += count($valueCourseNature);
							}
					?>
							<td rowspan='<?php echo $coursesNumber[$keyModule]; ?>'><?php echo $keyModule; ?></td>
					<?php
							foreach ($valueArrayIntervenant as $keyIntervenant => $valueArrayNature) {
								//Intervenant
								$firstNature = true;
								if ($firstIntervenant == false){
									//If not first time, create new row
					?>
									<tr>
					<?php
								}
								$firstIntervenant = false;
					?>
								<td rowspan='<?php echo count($valueArrayNature); ?>'><?php echo $keyIntervenant; ?></td>
					<?php

								foreach ($valueArrayNature as $keyNature => $valueNature) {
									//CM - TD - TP
									$idDiv = trim_string($keyModule) .'_'. trim_string($keyIntervenant) .'_LSI1_'. trim_string($keyNature);

									if ($firstNature == false){
										//If not first time, create new row
					?>
										<tr>
					<?php
									}
									$firstNature = false;
					?>
									<td class="type"><?php echo $keyNature; ?></td>
					<?php
									if ($modification == true){
					?>
										<td id="<?php echo $idDiv; ?>" class="quantity">
					<?php
										for ($i = 1; $i <= 5; $i += 1) {
					?>
											<div class="course" id="<?php echo $idDiv .'_item'. $i ?>" draggable="true"></div>
					<?php
										}
					?>				
										</td>
					<?php
									}
									//$numberWeekNumbers = 0 ;
									foreach($weekNumbers as $keyWeekNumber => $valueHoursLeft){
										if ($valueHoursLeft == '0') {
					?>
											<td class='full'>
											<div>
					<?php
										} else {
											$squareBusy = false;
											if (array_key_exists($keyNature, $valueArrayNature)){
												//Get the array (numSemaine => heures) of the course nature
												$valueArraySemaine = $valueArrayNature[$keyNature];

												if (array_key_exists($keyWeekNumber, $valueArraySemaine)){
													//Display the number of hours
													if ($valueArraySemaine[$keyWeekNumber] != 0) {
														$squareBusy = true;
													}
												}
											}
											 /*if(isset($bgcolor[$keyIntervenant][$numberWeekNumbers])){

                            					 $bgcolorCss = $bgcolor[$keyIntervenant][$numberWeekNumbers];
                   ?>
                    							<td bgcolor='<?php echo $bgcolorCss?>'><?php

	                               			 } else {
                    ?> 
                    							<td>
                    <?php
	                            }*/
					?>
											<td>
											<div class='<?php if($squareBusy) echo 'squareBusy'; ?> week <?php echo 'week'. $keyWeekNumber; ?>' id='<?php echo $idDiv .'_week'. $keyWeekNumber; ?>'>
					<?php
										}
										if ($squareBusy) {
											if ($modification) {
												for ($i = 0; $i < $valueArraySemaine[$keyWeekNumber]; $i += 1) {
					?>
													<div class="course" id="<?php echo $idDiv .'_meti'. $i ?>" draggable="true"></div>
					<?php
												}
					?>
												<input class="input_hoursToRemove" name="input_hoursToRemove" type="hidden" value="0" >
					<?php
											} else {
												echo $valueArraySemaine[$keyWeekNumber];
											}
										}
					?>
										</div>
										</td>
					<?php
										//$numberWeekNumbers = $numberWeekNumbers + 1;
									}
								}
							}
					?>
							</tr>
					<?php
						}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<script>  
	function escapeRegExp(string) {
	    return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
	}

	function replaceAll(string, find, replace) {
	  return string.replace(new RegExp(escapeRegExp(find), 'g'), replace);
	}
	
	//===== Gestion du selecteur de planning.
	document.getElementById("filiereBtn").addEventListener('click', handleSchedule, false);

	//===== Gestion de l'export du planning
	var exportBtnElts = document.querySelectorAll('.exportBtn');
	[].forEach.call(exportBtnElts, function(exportBtnElt) {
		exportBtnElt.addEventListener('click', handleExport, false);
	});
	function handleExport(e) {
		//Get unrecorded addEventListener
		var dataJSON = getMarkers();

		//Get type of calendar format
		var calType = 0;
		switch (this.id) {
			case '<?php echo $id_vcal; ?>' :
				calType = 1;
				break;
			case '<?php echo $id_ical; ?>' :
				calType = 2;
				break;
			default :
				calType = 3;
		}

		//Get public
		vPublic = document.getElementById("publicSelected").value;

		//Request page for download
		document.location.assign("<?php echo $pathController . $controlerCal; ?>?dataJSON=" + JSON.stringify(dataJSON) + "&public=" + encodeURI(vPublic) + "&calType=" + calType);
	}

	//===== Gestion de la mise à jour avec le bouton de de commit/update.
	var commitBtnElt = document.getElementById("commitBtn");
	if (commitBtnElt != null) {
		commitBtnElt.addEventListener('click', handleCommit, false);
	}
	
	//===== Gestion de la mise à jour automatique.
	setInterval(handleCommit, 5 * 60 * 1000);	//Every 5 minutes

	function handleSchedule(e){
		var schedulesSelector = document.getElementById('filiereSelecteur');
		var selectedFiliere = schedulesSelector.options[schedulesSelector.selectedIndex].value;

		window.location.assign("<?php echo $pathController . $controlerSelf .'?'. $getParamName ?>=" + encodeURI(selectedFiliere));
	}

	function handleCommit(e) {
		//Get element to add hours
		var dataJSONadd = getMarkers(true);
		//Get element to retrieve hours
		var dataJSONsup = getMarkers(false);
		//Add hours
		$.ajax({
			url : "<?php echo $pathController . $controlerAdd; ?>",		
			type : 'POST',
			data : {dataJSONadd: dataJSONadd, dataJSONsup: dataJSONsup},
			dataType : 'json', // On désire recevoir du HTML
			success : function(JsonResponse, statut){
				if (JsonResponse == true) {
					var markers = document.querySelectorAll('.marker');
					[].forEach.call(markers, function(marker) {
						//Remoce marker
						//marker.parentNode.removeChild(marker);

						//Remove class marker (=no more change waitting to be committed)
						marker.classList.remove('marker');

						//Clone marker into the bin if comming from the bin
						var idMarkerQuantityDiv = marker.id.substring(0, marker.id.lastIndexOf('_'));
						if ((marker.id.substring(marker.id.lastIndexOf('_') + 1, marker.id.length)).substring(0, 4) == "item") {
							var clone = marker.cloneNode(true);
							document.getElementById(idMarkerQuantityDiv).appendChild(clone);
						}

						//Counter hours to retrieve
						var markers = document.querySelectorAll('.input_hoursToRemove');
						[].forEach.call(markers, function(marker) {
							marker.value = 0;
						});
					});
				}
			}
		});
	}
		
	//===== Récupère les ajouts non encore enregistré
	//false => ajout heures
	//true => supprime heures
	function getMarkers(ajout) {
		if (ajout == "undefined" || ajout == true) {
			ajout = true;
		} else {
			ajout = false;
		} 

		var arrayId = [];
		var arrayNew = [];
		var markers;
		if (ajout == true) {
			markers = document.querySelectorAll('.marker');
		} else {
			markers = document.querySelectorAll('.input_hoursToRemove');
		}
		[].forEach.call(markers, function(marker) {
			if (ajout == true || marker.value != 0) {
				//Replace '___' by ' ' (changed because space not allowed in id)
				var dataMarker = replaceAll(marker.parentNode.id, '___', ' ').split('_');

				if (dataMarker.length != 5) {
					console.log("Data slit error!");
					return;
				}
				dataMarker[4] = replaceAll(dataMarker[4], 'week', '');
				arrayId.push(marker.parentNode.id);

				if (ajout == false) {
					for ($ii = 0; $ii < marker.value; $ii += 1) {
						arrayNew.push(dataMarker);
					}
				} else {
					arrayNew.push(dataMarker);
				}
			}
		});

		//Format data in JSON string
		var dataString = '{ "markers": [ ';
		for (var key in arrayNew) {
			dataString += '{';
			dataString += '"module": "' + arrayNew[key][0] + '",'
			dataString += '"intervenant": "' + arrayNew[key][1] + '",'
			dataString += '"filiere": "' + arrayNew[key][2] + '",'
			dataString += '"nature": "' + arrayNew[key][3] + '",'
			dataString += '"semaine": "' + arrayNew[key][4] + '"'
			dataString += '},';
		}
		
		//Remove the last coma (space if arrayNew empty)
		dataString = dataString.substring(0, dataString.length - 1);
		dataString += ' ]}';
		return JSON.parse(dataString);
	}

	//===== Gestion du glisser-deposer
	var dragSrcEl = null;
	var dragSrcElParentNode = null;

	function handleDragStart(e) {
		//this.style.opacity = '0.4';  // this / e.target is the source node.
		dragSrcEl = this;
		dragSrcElParentNode = this.parentNode;

		e.dataTransfer.effectAllowed = 'move';
		e.dataTransfer.setData('text/plain', this.id);
	}
	
	function handleDragOver(e) {
		if (e.preventDefault) {
			e.preventDefault(); // Necessary. Allows us to drop.
		}
			
		e.dataTransfer.dropEffect = 'move';  // See the section on the DataTransfer object.

		return false;
	}

	function handleDragEnter(e) {
		if (e.preventDefault) {
			e.preventDefault(); // Necessary. Allows us to drop.
		}
			
		var sourceElemId = dragSrcEl.id.substring(0, dragSrcEl.id.lastIndexOf('_'));
		var targetElemId = this.id.substring(0,this.id.lastIndexOf('_'));

		if(sourceElemId == targetElemId){
			// this / e.target is the current hover target.
			this.classList.add('over');	
		}
	}

	function handleDragLeave(e) {
		var sourceElemId = dragSrcEl.id.substring(0, dragSrcEl.id.lastIndexOf('_'));
		var targetElemId = this.id.substring(0,this.id.lastIndexOf('_'));

		if(sourceElemId == targetElemId){
			this.classList.remove('over');  // this / e.target is previous target element.
		}
	}
	
	function handleDrop(e) {
		// this / e.target is current target element.
		if(e.preventDefault) {
			e.preventDefault();
		}
		if (e.stopPropagation) {
			e.stopPropagation(); // stops the browser from redirecting.
		}

		//Compare string from first character to last occurence of '_'
		var sourceElemId = dragSrcEl.id.substring(0, dragSrcEl.id.lastIndexOf('_'));
		var targetElemId = this.id.substring(0,this.id.lastIndexOf('_'));

		if(sourceElemId == targetElemId){
			var parentNode = dragSrcEl.parentNode;
			var ref = parentNode.removeChild(dragSrcEl);
			ref.classList.add('marker');
			this.appendChild(ref);

			//DROP TARGET : Update hours left in th element
			if (dragSrcElParentNode.className.indexOf("squareBusy") > -1) {
				var inputElt = dragSrcElParentNode.getElementsByTagName("input");

				if (inputElt.length > 0) {
					inputElt[0].value = parseInt(inputElt[0].value) + 1;
				}
			}
			var weekId = dragSrcEl.parentNode.id.substring(dragSrcEl.parentNode.id.lastIndexOf('_') + 1, dragSrcEl.parentNode.id.length);
			var weekElt = document.getElementById('header' + weekId);
			var weekIdContent = weekElt.innerHTML;
			weekElt.innerHTML = weekIdContent - 1;
			//If week full
			if (weekElt.innerHTML == '0') {
				//Remove drag&drop on td AND add class full
				var weeksClass = document.querySelectorAll('.' + weekId);
				[].forEach.call(weeksClass, function(weekClass) {
					weekClass.classList.remove('week');
					weekClass.parentNode.classList.add('full');
				});
			}

			//DRAG SOURCE : Update hours left in th element and enable drag&drop if classe name begin with 'week'
			if (dragSrcElParentNode.id != 'undefined') {
				var weekId = dragSrcElParentNode.id.substring(dragSrcElParentNode.id.lastIndexOf('_') + 1, dragSrcElParentNode.id.length);
		
				if (weekId.indexOf('week') == 0) {
					var weekElt = document.getElementById('header' + weekId);
					var weekIdContent = weekElt.innerHTML;
					weekElt.innerHTML = parseInt(weekIdContent) + 1;
					//If week full
					if (weekElt.innerHTML == '1') {
						//Remove drag&drop on td AND add class full
						var weeksClass = document.querySelectorAll('.' + weekId);
						[].forEach.call(weeksClass, function(weekClass) {
							weekClass.classList.add('week');
							weekClass.parentNode.classList.remove('full');
						});
					}
				}
			}
		}
		return false;
	}

	function handleDragEnd(e) {
	// this/e.target is the source node.
		[].forEach.call(weeks, function (week) {
			week.classList.remove('over');
		});
	}
		
	var courses = document.querySelectorAll('.course');
	[].forEach.call(courses, function(course) {
		course.addEventListener('dragstart', handleDragStart, false);
		course.addEventListener('dragend', handleDragEnd, false);
	});
	
	var weeks = document.querySelectorAll('.week');
	[].forEach.call(weeks, function(week) {
		week.addEventListener('dragenter', handleDragEnter, false);
		week.addEventListener('dragover', handleDragOver, false);
		week.addEventListener('dragleave', handleDragLeave, false);
		week.addEventListener('drop', handleDrop, false);
	});   
</script>
