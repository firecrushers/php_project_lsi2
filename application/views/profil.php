<?php
if (get_called_class() != 'Controller') {
	die('Access denied!');
}
?>
	<ul class="nav navbar-nav navbar-left">
		<li><a id="planningBtn">Planning</a></li>
	</ul>
<?php
	include __DIR__ .'/layout/Component/userMenuEnd.php';
?>
<h1> Profil : <?php echo $myLogin; ?> </h1>
<br />
<?php
$arrayProfil = array(
					array(
						'text' => 'Mot de passe',
						'value' => $dataPwd,
						'id' => 'inputMdp',
						'editable' => 'false',
						'type' => 'password'),
					array(
						'text' => 'Nom',
						'value' => $dataNom,
						'id' => 'inputNom',
						'editable' => 'false',
						'type' => 'text'),
					array(
						'text' => 'Prénom',
						'value' => $dataPrenom,
						'id' => 'inputPrenom',
						'editable' => 'false',
						'type' => 'text'),
					array(
						'text' => 'Statut',
						'value' => $dataStatut,
						'id' => 'inputStatut',
						'editable' => 'true',
						'type' => 'text'),
					array(
						'text' => 'Statutaire',
						'value' => $dataStatutaire,
						'id' => 'inputStatutaire',
						'editable' => 'true',
						'type' => 'text'),
					array(
						'text' => 'Actif',
						'value' => $dataActif,
						'id' => 'inputActif',
						'editable' => 'true',
						'type' => 'text'),
					array(
						'text' => 'Fonction',
						'value' => $dataAdministrateur,
						'id' => 'inputFonction',
						'editable' => 'true',
						'type' => 'text')
				);
?>
<div class="jumbotron">
	<form name='formProfil' method="post" onsubmit="return checkField();">
	<?php
		foreach ($arrayProfil  as $arrayProfilKey) {
	?>
	<div class='row rowProfil'>
		<div class='col-xs-3'>
			<?php echo $arrayProfilKey['text']; ?>
		</div>
		<div class='col-xs-9'>
			<input name='<?php echo $arrayProfilKey['id']; ?>' class='form-control' type='<?php echo $arrayProfilKey['type']; ?>' id='<?php echo $arrayProfilKey['id']; ?>' value="<?php echo $arrayProfilKey['value']; ?>" <?php if ($arrayProfilKey['editable'] == 'true') { echo 'readonly'; } ?>>
		</div>
	</div>
	<?php
		}
	?>
	<div class='row rowProfil' style="text-align: center;">
		<input class='btn btn-success' type='submit' value="Enregistrer">
	</div>
	</form>
</div>
<script>
	function checkField(){
		if (document.formProfil.inputFonction.value.trim() != '') {
			document.formProfil.submit();
		}
		return false;
	}
</script>