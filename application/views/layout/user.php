<?php
if (@get_called_class() != 'Controller') {
	die('Access denied!');
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
	<?php
    	include __DIR__ .'/Component/head.php';
    ?>
  </head>
  <body>
    <?php
    	include __DIR__ .'/Component/userMenuStart.php';
    	echo $contentForLayout;
    ?>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
     <?php
    	include __DIR__ .'/Component/script.php';
    ?>
  </body>
</html>