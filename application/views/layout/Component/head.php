<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="./images/favicon.ico">

<title><?php echo $titlePage; ?></title>

<!-- Bootstrap core CSS -->
<link href="<?php echo $pathLibrary; ?>/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $pathLibrary; ?>/css/bootstrap-select.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="<?php echo $pathLibrary; ?>/css/dashboard.css" rel="stylesheet">

<style>
	/*Profil*/
	.rowProfil {
		max-width: 70%;
		margin: 10px auto;
	}

	div.course{
		height:10px;
		width:5px;
		background:red;
		margin:2px;
		display:inline-block;
	}
	.quantity{
		
	}
	div.week{
		height:50px;
		width:50px;
	}
	div.week.over {
		border: 2px dashed #000;
	}

	.full {
		background-color: #FF5050;
	}
	.table th {
		text-align: center;
	}
	.hoursLeft {
		color: green;
	}
	.marginb20 {
		margin-bottom: 10px;
	}
	.input-group {
		width: 100%;
	}
	#container {
		max-width: 500px;
		margin: 0 auto;
		font-size: 20px;
	}

	@media (max-width: 900px) {
	    .navbar-header {
	        float: none;
	    }
	    .navbar-left,.navbar-right {
	        float: none !important;
	    }
	    .navbar-toggle {
	        display: block;
	    }
	    .navbar-collapse {
	        border-top: 1px solid transparent;
	        box-shadow: inset 0 1px 0 rgba(255,255,255,0.1);
	    }
	    .navbar-fixed-top {
			top: 0;
			border-width: 0 0 1px;
		}
	    .navbar-collapse.collapse {
	        display: none!important;
	    }
	    .navbar-nav {
	        float: none!important;
			margin-top: 7.5px;
		}
		.navbar-nav>li {
	        float: none;
	    }
	    .navbar-nav>li>a {
	        padding-top: 10px;
	        padding-bottom: 10px;
	    }
	    .collapse.in{
	  		display:block !important;
		}
	}
</style>