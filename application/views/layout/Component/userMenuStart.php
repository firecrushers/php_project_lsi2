<?php
if (@get_called_class() != 'Controller') {
	die('Access denied!');
}
?>
<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		<a class="navbar-brand" href="#"><?php echo $navTitle; ?></a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-right">
			<?php 
				if ($DDE) {
			?>
				<li><a id="editModuleBtn" href="javascript:void(0);">Editer module</a></li>
			<?php
				}
				if ($Responsable) {
			?>
				<li><a id="chargeTeachBtn" href="javascript:void(0);">Charge enseignants</a></li>
			<?php
				}
				if (LOGIN != '') {
			?>
				<li>
					<form class="navbar-form">
						<button id="deconnexionBtn" type="button" name="deconnexion" class="btn btn-success">
							Deconnexion
						</button>
						<button id="profilBtn" type="button" name="profil" class="btn btn-success">
							Profil
						</button>
					</form>
				</li>
			</ul>
			<?php
				} else {
			?>
				<li>
					<form class="navbar-form">
						<button id="connexionBtn" type="button" name="connexion" class="btn btn-warning">
							Connexion
						</button>
					</form>
				</li>
			</ul>
				
			<?php
				}
			?>