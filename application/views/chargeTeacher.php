<?php
if (get_called_class() != 'Controller') {
	die('Access denied!');
}
?>
	<ul class="nav navbar-nav navbar-left">
		<li><a id="planningBtn">Planning</a></li>
	</ul>
<?php
	include __DIR__ .'/layout/Component/userMenuEnd.php';
?>
<h1> Charge de tous les enseignants : </h1>
<br />


<div class="container-fluid">
  <div class="row">     
	<div class="table-responsive">
        <table class="table table-striped">
			<thead>
			<tr>
 			<th>Enseignant</th>
            <?php
            foreach($weekNumber as $week){
              ?>
              <th>Semaine <?php echo $week ?></th>
              <?php
            }
            ?>
			</tr>
			</thead>
				<tbody>
				<?php
					//Number of rows per teacher
					$coursesNumber = array();
					//Boolean to check if week is at the beginning of the row (if so then new row is requiered)
					$firstWeek = true;
					
                                        //Chaque professeur
					foreach ($chargeTeacher as $teacher => $arrayWeek) {
				?>
						<tr>
						<td><?php echo $teacher; ?></td>
				<?php
						foreach ($weekNumber as $week) {
                                                    if(isset($arrayWeek[$week])){
                                                        ?><td  style="text-align:center" ><?php echo $arrayWeek[$week]; ?></td><?php
                                                    }else {
                                                        ?><td></td><?php
                                                    }
						}
				?>					
						</tr>
				<?php
					}
				?>
				</tbody>
			</table>
		</div>
	</div>
</div>
