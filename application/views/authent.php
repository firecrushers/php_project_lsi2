<?php
//if (get_called_class() != 'Controller') {
//	die('Access denied!');
//}
?>
<div id='container'>
	<div id="message" ></div>
	<h2 style="text-align: center; color: #5bc0de;">Authentification</h2>
	<form
		id='formAuthent'
		name="formAuthent"
		onSubmit="return submitFormAuthent();"
		METHOD="POST">
		<div class='jumbotron modal-content' style='padding: 30px; background-color: white; margin-bottom: 15px; border-radius: 4%;'>
			<div class='row marginb20'>
				<div class='col-xs-4' style="color: #C0C0C0;">
					Id
				</div>
				<div class='col-xs-8'>
					<div class='input-group'>
						<input class='form-control' NAME="login" id="login">
					</div>
				</div>
			</div>
			<div class='row marginb20'>
				<div class='col-xs-4' style="color: #C0C0C0;">
					Mot de passe
				</div>
				<div class='col-xs-8'>
					<div class='input-group'>
						<input class='form-control' TYPE=password NAME="pwd" id="pwd">
					</div>
				</div>
			</div>
		</div>
		<input class='btn btn-lg btn-info' TYPE='SUBMIT' VALUE="Connexion">
		<input id='planningBtn' class='btn btn-lg btn-warning' TYPE='button' VALUE="Planning">
	</form>
</div>
<script>
function submitFormAuthent() {
	var login = $("#login").val();
	var pwd = $("#pwd").val();

	if (login.trim() != '' && pwd.trim() != '') {
		var myData = '{"id": [{"login": "' + login + '","pwd": "' + pwd + '"}]}';
		var JSONdata = JSON.parse(myData);

		$.ajax({
	   		type: "POST", 
	   		url: "<?php echo $pathController; ?>/ajax/co.php",//URL du controlleur 
	   		data: JSONdata,
			dataType : 'json',
			success: function(JsonResponse, statut) {
				if (JsonResponse.state == "Success"){
					location.reload(true); //On est connecté donc on rafraîchit la page
				} else {
					$("#message").empty();
					$("#message").append("Les identifiants ne sont pas corrects ou le serveur est inaccessible.");
					$("#login").css({border:"2px solid red"}); //On indique qu'il y a une erreur
					$("#pwd").css({border:"2px solid red"});
				} 
			},
			error: function(){
				console.log("Pas reçu");
			}
		});
	}

	//Avoid redirection from form
	return false;
}
</script>
