<?php
	require_once  __DIR__. '/Model.php';

	class DBplannings extends Model {
		function DataBase() {
			Model::model();
		}

		protected function connexion(){
			$dns = 'mysql:host=127.0.0.1;dbname=PLANNINGS';
			$utilisateur = 'planningDb';
			$motDePasse = 'PlanningRobot';
			try {
			  $connection = new PDO( $dns, $utilisateur, $motDePasse );
			} catch ( Exception $e ) {
			        echo 'Connection à MySQL impossible :' , $e->getMessage();
			        die();
			}
			return $connection;
		}

      	/**
		*	Retourne un tableau contenant les numéros de semaine classé par ordre croissant
		**/
		function getAllWeekNumbers(){
			$query = 'SELECT Semaine.semaine
						FROM Semaine
						ORDER BY Semaine.semaine ASC';
			$select = $this->database->prepare($query);
			$select->setFetchMode(PDO::FETCH_OBJ);
			$select->execute();
			$result = $select->fetchAll();

			$resultSorted = array();
			foreach ($result as $value) {
				$resultSorted[] = $value->semaine;
			}
			return $resultSorted;
		}

		/*
		*	Retourne les informations de l'utilisateur sous forme d'un tableau associatif
		*	['pwd' => pwd,
		*		'nom' => nom,
		*		'prenom' => prenom,
		*		'statut' => statut,
		*		'statutaire' => statutaire,
		*		'actif' => actif,
		*		'administrateur' => administrateur]
		*/
		function getDataProfil($login = '') {
			$query = 'SELECT *
						FROM Enseignant
						WHERE login="'. $login .'"';
			$select = $this->database->prepare($query);
			$select->setFetchMode(PDO::FETCH_OBJ);	
			$select->execute() or die('Query error : getDataProfil');
			$result = $select->fetch();

			$resultFormatted = array();
			$resultFormatted['pwd'] = $result->pwd;
			$resultFormatted['nom'] = $result->nom;
			$resultFormatted['prenom'] = $result->prenom;
			$resultFormatted['statut'] = $result->statut;
			$resultFormatted['statutaire'] = $result->statutaire;
			if ($result->actif == '1') {
				$resultFormatted['actif'] = "oui";
			} else {
				$resultFormatted['actif'] = "non";
			}
			if ($result->administrateur == '1') {
				$resultFormatted['administrateur'] = addslashes("direction des études");
			} else if ($result->administrateur == '2') {
				$resultFormatted['administrateur'] = addslashes("responsable d'année");
			} else {
				$resultFormatted['administrateur'] = addslashes("utilisateur");
			}

			return $resultFormatted;
		}

		/*
		*	Update password from user $login
		*/
		function setPwdProfil($login = '', $pwd = 'serviceEnssat') {
			$sql = 'UPDATE Enseignant SET pwd="'. $pwd .'" WHERE login="'. $login .'"';
			$select = $this->database->prepare($sql);
			$select->setFetchMode(PDO::FETCH_OBJ);
			$select->execute() or die('Query error : setPwdProfil');

			return true;
		}

		/*
		*	Update nom from user $login
		*/
		function setNomProfil($login , $nom )
		{
			$sql = 'UPDATE Enseignant SET nom="' . $nom . '" WHERE login="' . $login . '";';
			$select = $this->database->prepare($sql);
			$select->setFetchMode(PDO::FETCH_OBJ);
			$select->execute() or die('Query error : setNomProfil');
			$result = $select->fetchAll();

			return true;
		}

		/*
		*	Update prenom from user $login
		*/
		function setPrenomProfil($login, $prenom)
		{
			$sql = 'UPDATE Enseignant SET prenom="' . $prenom . '" WHERE login="' . $login . '";';
			$select = $this->database->prepare($sql);
			$select->setFetchMode(PDO::FETCH_OBJ);
			$select->execute() or die('Query error : setPrenomProfil');
			$result = $select->fetchAll();

			return true;
		}

		/*
		*	Retourne un tableau associatif avec enseignant => semaine => couleur où couleur indique la
		*	la charge de l'enseignant pour la semaine associée
		*/
		public function colorChargeTeacherAll(){
			$sql = 'SELECT B.nbHeures, A.enseignant, B.semaine FROM ContenuModule A INNER JOIN AffectationSemaine B ON A.module=B.module AND A.partie=B.partie WHERE A.enseignant!= \'null\'';
			$select = $this->database->prepare($sql);
			$select->setFetchMode(PDO::FETCH_OBJ);
			$select->execute() or die('Query error : chargeTeacherAll');
			$result = $select->fetchAll();
			$resultSorted = array();
			//rangement des données
			foreach ($result as $value) {
				$resultSorted[$value->enseignant][$value->semaine] = $value->nbHeures;
			}
			$colorStored = array();
			$counterEnseignant = 0;
			//association de la couleur en fonction du nombre d'heure par semaine
			foreach($resultSorted as $enseignant){
				$keyEnseignant = array_keys($resultSorted);
				foreach($enseignant as $semaine=>$heures){
					if($heures > 20){
						//code rouge
						$colorStored[$keyEnseignant[$counterEnseignant]][$semaine] = '#FF0000';
					}
					elseif($heures > 10){
						//code orange
						$colorStored[$keyEnseignant[$counterEnseignant]][$semaine] = '#FF9900';
					}
					else {
						//code vert
						$colorStored[$keyEnseignant[$counterEnseignant]][$semaine] = '#33CC00';
					}
				}
				$counterEnseignant = $counterEnseignant+1;
			}
			return $colorStored;
		}
		
		/**
		*	Retourne un tableau contenant les numéros de semaine classé par ordre croissant
		**/
		function getWeekNumbers(){

			$query = '(SELECT A.semaine, HeuresAffectees, nombreHeuresMax
					FROM affectationsparsemaine B 
					INNER JOIN Semaine A
					ON A.semaine = B.semaine)
					UNION
					(SELECT A.semaine, HeuresAffectees, nombreHeuresMax
					FROM affectationsparsemaine B
					RIGHT JOIN Semaine A
					ON A.semaine = B.semaine
					WHERE B.semaine IS NULL)
					ORDER BY semaine ASC';
			$select = $this->database->prepare($query);
			$select->setFetchMode(PDO::FETCH_OBJ);
			$select->execute() or die('Query error : getWeekNumbers');
			$result = $select->fetchAll();

			$resultSorted = array();
			foreach ($result as $value) {
				$resultSorted[$value->semaine]['nbHeuresMax'] = $value->nombreHeuresMax;
				$resultSorted[$value->semaine]['heuresAffectees'] = $value->HeuresAffectees;
			}
			return $resultSorted;
		}

		/**
		*   $filiere est la filière dont on veut l'emploi du temps
		*	Retourne le tableau Module (clé => valeur)
		*		Module (nom => tableau d'Enseignant)
		*		Enseignant (nom => tableau de Type)
		*		Type (nom => tableau de numSemaine)
		*		numSemaine (numero => nombre d'heure)
		**/
		public function getSchedule($filiere = '*'){
			$whereClause = '';
			if ($filiere != '*') {
				$whereClause = "WHERE M.public = '". $filiere ."'";
			}

			$query = 'SELECT * FROM
			(

					(
					SELECT A.module, A.semaine, A.nbHeures, B.partie, B.enseignant 
					FROM AffectationSemaine A 
					INNER JOIN ContenuModule B
					ON A.module = B.module AND A.partie=B.partie
					ORDER BY A.module, B.partie ASC
				) UNION (
					SELECT C.module, 1 as semaine, 0 as nbHeures, C.partie, C.enseignant
					FROM ContenuModule C
					WHERE (C.module, C.partie)
					NOT	IN (
						SELECT D.module, D.partie
						FROM AffectationSemaine D
					)
					ORDER BY C.module, C.partie ASC
					)
			) T
			INNER JOIN Module M
			on T.module = M.module '. $whereClause;

			$select = $this->database->prepare($query);
			$select->setFetchMode(PDO::FETCH_OBJ);
			$select->execute() or die('Query error : getSchedule');
			$result = $select->fetchAll();

			$resultSorted = array();
			foreach ($result as $value) {
				if ($value->enseignant == '') $value->enseignant = "Non attribué";
				$resultSorted[$value->module][$value->enseignant][$value->partie][$value->semaine] = $value->nbHeures;
			}
			return $resultSorted;
		}

		/*
		*	Fonction qui test si l'utilisateur est un responsable d'année
		* 	renvoie un booleen
		*/
		public function isResponsible($login){
			# $public n'est pas utilise car la base de donnee ne nous permet de pas tester la filliaire du responsable d'annee.
			$success = false;
			$sql = 'SELECT administrateur FROM Enseignant WHERE login=\''.$login.'\'';
			#Preparation de la requete
			$requete = $this->database -> prepare($sql);
			#Parametrage du tye de retour du fetch
			$requete -> fetch(PDO::FETCH_OBJ);
			#execution de la requette
			$requete -> execute() or die('Query error : isResponsible');
			$data = $requete -> fetch();
			#Verification de la requête
			if($data['administrateur'] == 2) {
				$success = true;
			}
			return $success;
		}
		
		/*
		/	Fonction qui test si l'utilisateur est responsable de la promotion 'public' d'année
		*	renvoie un booleen
		*	Non implémentable en raison de la BDD (fonction isResponsible en attendant)
		*/
		public function isResponsibleWith($login, $public){
			return $this->isResponsible($login);
		}

		public function isDDE($login = '')
		{
			$return = false;

			$sql = 'SELECT administrateur FROM Enseignant WHERE login="' . $login . '";';
			$select = $this->database->prepare($sql);
			$select->setFetchMode(PDO::FETCH_ASSOC);
			$select->execute() or die('Query error : isDDE');
			$result = $select->fetch();

			if ($result['administrateur'] == 1)	{
				$return = true;
			}
			return $return;
		}

		//Fonction qui donne la charge de tout les enseignants 
		public function chargeTeacherAll(){
			$sql = 'SELECT B.nbHeures, A.enseignant, B.semaine 
					FROM ContenuModule A
					INNER JOIN AffectationSemaine B 
					ON A.module=B.module AND A.partie=B.partie';

			$select = $this->database->prepare($sql);
			$select->setFetchMode(PDO::FETCH_OBJ);
			$select->execute() or die('Query error : chargeTeacherAll');
			$result = $select->fetchAll();

			$resultSorted = array();
			foreach ($result as $value) {
				if ($value->enseignant == '') $value->enseignant = 'Non attribué';
				$resultSorted[$value->enseignant][$value->semaine] = $value->nbHeures;
			}
			return $resultSorted;
		}

		/**
		*	Retourne un tableau associatif:
		*	key => Values
		*	NomModule => Array(types de cours; ex: CM, TP, TD ...)
		*	Note: Possibilité de regrouper les commandes prepare, set fetch mode , execute en une fonction demandeQuery($query) ?
		*/
		public function getModuleCourseType($filiere){
			$sql = 'SELECT module, partie FROM ContenuModule ORDER BY partie ASC';
			$select = $this->database->prepare($sql);
			$select->setFetchMode(PDO::FETCH_OBJ);
			$select->execute() or die('Query error : getModuleCourseType');
			$result = $select->fetchAll();

			$resultSorted = array();
			foreach ($result as $value) {
				$resultSorted[$value->module][] = $value->partie;
			}

			return $resultSorted;
		}
		
		/** todo, se référer au doc Cas1 de gmail*/
		public function getPublic(){
			$sql = 'SELECT DISTINCT public FROM Module ORDER BY public ASC';
			$select = $this->database->prepare($sql);
			$select->setFetchMode(PDO::FETCH_OBJ);
			$select->execute() or die('Query error : getPublic');
			$result = $select->fetchAll();

			$resultSorted = array();
			foreach ($result as $value) {
				$resultSorted[] = $value->public;
			}

			return $resultSorted;
		}

		/*
		*	Rajoute dans la BDD un créneau de cours défini par les éléments passés en paramètre
		*
		*	Retourne un booleen, oui pour modification autorisé, non sinon.
		*
		*	Dans contenumodule :
		*	ajouter le nom de l'enseignant à la ligne correspondant.
		*	
		*	Dans affectationsemaine :
		*	ajouter une ligne pour le nouveau module avec la semaine correspondante.
		*	ou ajoute une heure à une ligne déjà existante
		*
		*/
		public function ajoutHoraire($module,$nature,$semaine){
			//Get hours already assigned to $module,$nature,$semaine
			$sql = 'SELECT nbHeures FROM affectationsemaine WHERE module="'. $module .'" AND partie="'. $nature .'" AND semaine="'. $semaine .'"';
			#Preparation de la requete
			$requete = $this->database -> prepare($sql);
			#Parametrage du tye de retour du fetch
			$requete -> fetch(PDO::FETCH_ASSOC);
			#execution de la requête
			$requete -> execute()  or die('Erreur de requete 1');
			$data = $requete -> fetch();
			$nbHeuresDejaPresentes = $data['nbHeures'];
			if ($nbHeuresDejaPresentes == '') $nbHeuresDejaPresentes = 0;

			//Get total hours of module
			$sql = 'SELECT HeuresAffectees FROM affectationsparmodulepartie WHERE module="'. $module .'" AND partie="'. $nature .'"';
			#Preparation de la requete
			$requete = $this->database -> prepare($sql);
			#Parametrage du tye de retour du fetch
			$requete -> fetch(PDO::FETCH_ASSOC);
			#execution de la requête
			$requete -> execute()  or die('Erreur de requete 2');
			$data = $requete -> fetch();
			$heuresAffectees = $data['HeuresAffectees'];
			if ($heuresAffectees == '') $heuresAffectees = 1;

			if($nbHeuresDejaPresentes + 1 <= $heuresAffectees){
				$success = true;
				$sql = 'SELECT (nbHeures+1) FROM affectationsemaine WHERE module = \''.$module.'\' AND	partie = \''.$nature.'\' AND semaine = \''.$semaine.'\'';
				#Preparation de la requete
				$requete = $this->database -> prepare($sql);
				#Parametrage du tye de retour du fetch
				$requete -> fetch(PDO::FETCH_ASSOC);
				#execution de la requête
				$requete -> execute()  or die('Erreur de requete 3');
				$data = $requete -> fetch();
				#Verification de la requête
				if($data == false){
					$ajouterLigne = true;
				} else {
					$ajouterLigne = false;
				}
				if($ajouterLigne){
					$sql = 'INSERT INTO affectationsemaine (module,partie,semaine,nbHeures) VALUES (\''.$module.'\',\''.$nature.'\',\''.$semaine.'\',\'1\')';
				} else {
					$sql = 'UPDATE affectationsemaine SET nbHeures = \''.$data['(nbHeures+1)'].'\' WHERE module = \''.$module.'\' AND	partie = \''.$nature.'\' AND semaine = \''.$semaine.'\'';
				}
				#Preparation de la requete
				$requete = $this->database -> prepare($sql);
				#Parametrage du tye de retour du fetch
				$requete -> fetch(PDO::FETCH_ASSOC);
				#execution de la requête
				$requete -> execute()  or die('Erreur de requete 4');
				$data = $requete -> fetch();
			} else {
				$success = false;
			}
			return $success;
		}
		
		public function supprimeHoraire($module,$nature,$semaine){
			$sql = 'SELECT nbHeures FROM affectationSemaine WHERE module="'. $module .'" AND partie="'. $nature .'" AND semaine="'. $semaine . '"';
			$select = $this->database->prepare($sql);
			$select->setFetchMode(PDO::FETCH_ASSOC);
			$select->execute() or die('Query error : supprimeHoraire SELECT');
			$result = $select->fetch();
		
			$sql = 'UPDATE affectationSemaine SET nbHeures="'. ($result['nbHeures']-1) .'" WHERE module="'. $module .'" AND partie="'. $nature .'" AND semaine="'. $semaine . '"';
			$select = $this->database->prepare($sql);
			$select->execute() or die('Query error : supprimeHoraire UPDATE');
		}

		/*
		*	Fonction qui test l'authenticité de l'utilisateur
		* 	renvoie une booleen pour l'authentification ainsi que le status si l'authentification a reussie.
		*/
		public function authentification($login,$password){
			$success = false;
			$status = 0;
			$sql = 'SELECT login,pwd,administrateur FROM Enseignant WHERE login=\''.$login.'\' AND pwd=\''.$password.'\' ';
			#Preparation de la requete
			$requete = $this->database -> prepare($sql);
			#Parametrage du tye de retour du fetch
			$requete -> fetch(PDO::FETCH_OBJ);
			#execution de la requette
			$requete -> execute() or die('Query error : authentification');
			$data = $requete -> fetch();
			#Verification de la requête
			if(!empty($data)){
				$success = true;
				#On n'a acces au status de l'utilisateur seulement c'est c'est vraiment lui
				$status = $data['administrateur'];
			}
			$retour = array(
				"success" => $success,
				"status" => $status
			);
			return $retour;
		}

		/* Permet d'update le responsable d'un module donné
		*/
		public function UpdateRespo($module, $newResponsable)
		{
			$sql = 'UPDATE Module SET responsable="'. $newResponsable .'" WHERE module="'. $module .'"';
			$select = $this->database->prepare($sql);
			$select->setFetchMode(PDO::FETCH_OBJ);
			$select->execute() or die('Query error : UpdateRespo');
			$select->fetchAll();

			return true;
		}
		
		/* Permet d'update le libellé d'un module donné
		*/
		public function UpdateLibelle( $module, $newLibelle)
		{
			$sql = 'UPDATE Module SET libelle="'. $newLibelle .'" WHERE module="'. $module .'"';
			$select = $this->database->prepare($sql);
			$select->setFetchMode(PDO::FETCH_OBJ);
			$select->execute() or die('Query error : UpdateLibelle');
			$select->fetchAll();

			return true;
		}
		
		/*	Fonction qui permet de récupérer le libellé d'un module
		*/
		public function getLibelle($module){
			$sql = 'SELECT libelle FROM Module WHERE module="'. $module .'"';
			$select = $this->database->prepare($sql);
			$select->setFetchMode(PDO::FETCH_OBJ);
			$select->execute() or die('Query error : getLibelle');
			$result = $select->fetchAll();
			
			return $result;
		}
		
		/*
		*	Renvoie à partir d'un module sa répartition définie sous la forme d'une imbrication de Array
		*	tel que défini dans le "magnifique" schéma ci-dessous !
		* (les $ indiquent des clé portant une information dans leur nom ... )
		*
		*[$module => 
        *		[$type =>
        *				[$partie =>
        *						[nbHeures => $nbHeures,
		*						hed => $hed,
		*						enseignant => $enseignant]
		* ]]]
		*/
		public function getRepartition($module)
		{
			$sql = 'SELECT partie, type, nbHeures, hed, enseignant FROM ContenuModule WHERE module="'. $module .'"';
			$select = $this->database->prepare($sql);
			$select->setFetchMode(PDO::FETCH_OBJ);
			$select->execute() or die('Query error : getLibelle');
			$result = $select->fetchAll();
			
			$resultSorted = array();
			
			foreach ($result as $value)
			{
				$partie= $value->partie;
				$hed= $value->hed;
				$enseignant= $value->enseignant;
				$nbHeures= $value->nbHeures;
				$type= $value->type;
				
				// Ligne ci dessous, j'espère qu'on peut faire aussi simple que ça ;)
				$resultSorted[$module][$type][$partie][]= Array ( "hed" => $hed , "nbHeures" => $nbHeures , "enseignant" => $enseignant );
			}

			return $resultSorted;
		}
		
		/* fais le contraire de getRepartition, avec un Array imbriqué sous même format.
		*	Efface au passage la partie AffectationSemaine dudit module !
		*/
		public function updateRepartition ($repartition)
		{
			foreach ($repartition as $module => $tableau1)	// un foreach, mais en pratique, y'aura qu'un module à la fois ;)
			{
				// Suppresssion des traces du module donné dans ContenuModule et dans AffectationSemaine
				$sql = 'DELETE FROM `affectationsemaine` WHERE module="'. $module .'";' ;
				$sql = $sql.'DELETE FROM `ContenuModule` WHERE module="'. $module .'";' ;
				
				$select = $this->database->prepare($sql);
				$select->setFetchMode(PDO::FETCH_OBJ);
				$select->execute() or die('Query error : updateRepartition / suppression module des tables');
				$select->fetchAll();
			
				$ajoutSQL="";
				foreach ($tableau1 as $type => $tableau2)
				{
					foreach ($tableau2 as $partie => $tableau3)
					{
						$hed= $tableau3["hed"];
						$enseignant= $tableau3["enseignant"];
						$nbHeures= $tableau3["nbHeures"];
						
						$ajoutSQL= $ajoutSQL. 'INSERT INTO `ContenuModule` (module,partie,type,nbHeures,hed,enseignant) VALUES ("'.$module.'","'.$partie.'","'.$type.'","'.$nbHeures.'","'.$hed.'","'.$enseignant.'");'."\n";
					}
				}
				
				$select = $this->database->prepare($ajoutSQL);
				$select->setFetchMode(PDO::FETCH_OBJ);
				$select->execute() or die('Query error : updateRepartition / Insertion decoupage');
				$select->fetchAll();
			}
			return true;
		}
	}
?>
