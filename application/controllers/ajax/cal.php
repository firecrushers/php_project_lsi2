<?php
require_once __DIR__. '/../../Controller.php';

$controller = new Controller();
	
	if (empty($_GET['public']) && empty($_GET['calType']) && empty($_GET['dataJSON'])) {
		die ('Wrong parameters');
	}
	$filiere = $_GET['public'];
	$calType = $_GET['calType'];
	$arrayValueToAdd = json_decode($_GET['dataJSON'])->markers;

	$fileName = 'myCalendar';
	$scheduleFormatted = array();
	if ($calType == '1') {
		//vCalendars
		header("Content-Type: text/x-vCalendar");
	    header("Content-Disposition: inline; filename=$fileName.vcs");
 	} else if ($calType == '2') {
	    //iCalendars
	    header("Content-Type: text/Calendar");
	    header("Content-Disposition: inline; filename=$fileName.ics");
	} else {
		//Coma Separated Value
	    header("Content-Type: text/plain");
	    header("Content-Disposition: attachement; filename=$fileName.csv");
	}

	//Get schedule from model
	$schedule = $controller->DBplannings->getSchedule($filiere);

	//Add course hours given in parameters to schedule
	foreach ($arrayValueToAdd as $valueToAdd) {
		if (
			array_key_exists($valueToAdd['module'], $schedule)
			&& array_key_exists($valueToAdd['intervenant'], $schedule[$valueToAdd['module']])
			&& array_key_exists($valueToAdd['nature'], $schedule[$valueToAdd['module']][$valueToAdd['intervenant']])
			&& array_key_exists($valueToAdd['semaine'], $schedule[$valueToAdd['module']][$valueToAdd['intervenant']][$valueToAdd['nature']])
			) {
			$schedule[$valueToAdd['module']][$valueToAdd['intervenant']][$valueToAdd['nature']][$valueToAdd['semaine']] += 1; 
		} else {
			$schedule[$valueToAdd['module']][$valueToAdd['intervenant']][$valueToAdd['nature']][$valueToAdd['semaine']] = 1;
		}
	}

	//Format for export
	foreach ($schedule as $moduleName => $arrayIntervenant) {
		
		foreach ($arrayIntervenant as $intervenantName => $arrayNature) {
			
			foreach ($arrayNature as $natureName => $arrayWeek) {
				
				foreach ($arrayWeek as $week => $hours) {

					if ($hours != '0') {
						$scheduleFormatted[] = array('module' => $moduleName,
													'intervenant' => $intervenantName,
													'nature' => $natureName,
													'week' => $week,
													'hours' => $hours);
					}
				}
			}
		}
	}
$output = "";
if ($calType == "1" || $calType == "2") {
	//If vCal or iCal
$output = "BEGIN:VCALENDAR
VERSION:1.0
PRODID:Enssat Web Calendar\n";

	foreach ($scheduleFormatted as $value) {
		$summary = $value['module'] .'-'. $value['nature'] .'-'. $value['intervenant'] .'-'. $filiere;
		$description = 'Semaine '. $value['week'] .', '. $value['hours'] .' heures';
		$valueWeek = $value['week'];

		//Week number starting from 01/09/2015 with number 1
		$date = new DateTime('2014-09-01');
		$daysToAdd = 7 * ($valueWeek - 1);
		$date->add(new DateInterval('P'. $daysToAdd .'D'));
		$descDump = str_replace("\r", "=0D=0A=", $description); 
	    //Format Ymd\THi00 --> '20020925T010000Z'
	    //Z at the end is optionnal : meaning UTC time
	    $start = $date->format('Ymd\THi00\Z');
	    $date->add(new DateInterval('PT23H'));
	    $end = $date->format('Ymd\THi00\Z');

$output .= "BEGIN:VEVENT
SUMMARY:$summary
DESCRIPTION;ENCODING=QUOTED-PRINTABLE:$descDump
DTSTART:$start
DTEND:$end
END:VEVENT\n";
	}
$output .= "END:VCALENDAR";
} else {
	//CSV format
	foreach ($scheduleFormatted as $value) {
		$output .= $value['module'];
		$output .= ",";
		$output .= $value['intervenant'];
		$output .= ",";
		$output .= $value['week'];
		$output .= ",";
		$output .= $value['hours'];
		$output .= "\r\n";
	}
}
	echo $output;
?>
