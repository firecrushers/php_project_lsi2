<?php
require_once __DIR__. '/../../Controller.php';

$controller = new Controller();

if(!empty($_POST["id"])){

	$id = $_POST["id"][0];

	$connexion = $controller->DBplannings->authentification($id["login"], $id["pwd"]);

	if($connexion['success']){ // teste la réussite de la connexion
		$_SESSION['login'] = $id["login"]; // variable qui vaut vrai si on est connecté
		 echo json_encode(array("state" => "Success"));
	} else { // Cas où l'identifcation échoue
		echo json_encode(array("state" => "Fail"));
	}
}

?>