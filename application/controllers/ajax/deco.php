<?php
	if (session_id() == '') {
		//If not connected
		session_start();
	}

	session_destroy();
	unset($_SESSION);

	echo json_encode(array('state' => 'Success'));
?>