<?php
if (!$_POST){
	die('Access denied!');
}
	require_once __DIR__. '/../../Controller.php';

	$controller = new Controller();

	$return = false;

	$toAdd = array();
	$toSup = array();
	$result = array();
	if (!empty($_POST['dataJSONadd'])) {
		$toAdd = $_POST['dataJSONadd']['markers'];
	}
	if (!empty($_POST['dataJSONsup'])) {
		$toSup = $_POST['dataJSONsup']['markers'];
	}

	foreach ($toAdd as $key => $value) {
		$result[$value['module']][$value['intervenant']][$value['nature']][$value['semaine']] = 0;
	}
	foreach ($toSup as $key => $value) {
		$result[$value['module']][$value['intervenant']][$value['nature']][$value['semaine']] = 0;
	}

	foreach ($toAdd as $key => $value) {
		$result[$value['module']][$value['intervenant']][$value['nature']][$value['semaine']] += 1;
	}
	foreach ($toSup as $key => $value) {
		$result[$value['module']][$value['intervenant']][$value['nature']][$value['semaine']] -= 1;
	}


	foreach ($result as $module => $arrayIntervenant) {
		foreach ($arrayIntervenant as $intervenant => $arrayNature) {
			foreach ($arrayNature as $nature => $arraySemaine) {
				foreach ($arraySemaine as $semaine => $heure) {
					if ($result[$module][$intervenant][$nature][$semaine] > 0) {
						$controller->DBplannings->ajoutHoraire($module,$nature,$semaine);
					} else if ($result[$module][$intervenant][$nature][$semaine] < 0) {
						$controller->DBplannings->supprimeHoraire($module,$nature,$semaine);
					}
				}
			}
		}
	}
	$return = true;
	echo json_encode($return);
?>