<?php
require_once __DIR__. '/../Controller.php';

if (!defined('LOGIN') || LOGIN == '') {
	//Visitor not logged or direct access by URL
	header("Location: ./planning.php");
	die();
}

$controller = new Controller();

	$responsible = $controller->DBplannings->isResponsible(LOGIN);

	$chargeTeacher = $controller->DBplannings->chargeTeacherAll();

	$weekNumbers = $controller->DBplannings->getAllWeekNumbers();

	$data = array('responsible' => $responsible,
				'chargeTeacher' => $chargeTeacher,
				'weekNumber' => $weekNumbers);

	//Sauvegarde des variables nécessaires à la vue
	$controller->set($data);

	$controller->setLayout('user');

	//Affichage de la vue
	$controller->render('Charge des enseignants', 'ChargeEnseignants', 'chargeTeacher');
?>
