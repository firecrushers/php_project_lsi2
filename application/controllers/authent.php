<?php
require_once __DIR__. '/../Controller.php';

	$controller = new Controller();

	if(defined('LOGIN') && LOGIN != ''){ // si on est connecté, on est redirigé vers le planning
		header("Location: ./planning.php");
		die();
	}

	//Affichage de la vue
	$controller->render('Planning par semaine', 'PlanningParSemaine', 'authent');
?>
