<?php
require_once __DIR__. '/../Controller.php';

$controller = new Controller();

	$getParamName = 'filiere';
	$getParamValueToAdd = 'Tout le monde';

	if(!empty($_GET[$getParamName])) {	
		if ($_GET[$getParamName] == $getParamValueToAdd) {
			$filiere = "*";
		} else {
			$filiere = $_GET[$getParamName];
		}
	} else {	// Si filière non spécifiée => toutes filières, définies par *
		$filiere = "*";
	}
	
	//Stock les couleurs de tous les profs
	$bgcolor = $controller->DBplannings->colorChargeTeacherAll();

	//demande le schedule, info dans /views/DBPlannings
	$schedule = $controller->DBplannings->getSchedule($filiere);
	
	//Get all week numbers available
	$weekNumbers = $controller->DBplannings->getWeekNumbers();
	
	//Indique l'ensemble des filières existantes	
	$publicList = $controller->DBplannings->getPublic();
	array_unshift($publicList, $getParamValueToAdd);
	
	// Indique si le planning peut être modifié par l'utilisateur courant.
	$modification = $controller->DBplannings->isResponsible(LOGIN);

	$teacherCharge = $controller->DBplannings->isResponsibleWith(LOGIN, $filiere);

	//Get file name
	$url = str_replace('\\', '/', __FILE__);
	$filename = substr($url, strrpos($url, '/'));

	$data = array('publicList'=>$publicList,
				'publicSelected'=>$filiere,
				'schedule' => $schedule,
				'bgcolor'=> $bgcolor,
				'weekNumbers' => $weekNumbers,
				'modification' => $modification,	
				'teacherCharge' => $teacherCharge,
				'getParamName' => $getParamName,
				'controlerSelf' => $filename,
				'controlerAdd' => '/ajax/task.php',
				'controlerCal' => '/ajax/cal.php');

	//Sauvegarde des variables nécessaires à la vue
	$controller->set($data);

	$controller->setLayout('user');

	//Affichage de la vue
	$controller->render('Planning par semaine', 'PlanningParSemaine', 'planning');
?>
