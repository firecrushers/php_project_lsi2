<?php
function trim_string($string = ''){
	return str_replace(' ', '', $string);
}

require_once __DIR__. '/../Controller.php';

if (!defined('LOGIN') || LOGIN == '') {
	//Visitor not logged or direct access by URL
	header("Location: ./planning.php");
	die();
}

$controller = new Controller();

	if (!empty($_POST['inputMdp']) && !empty($_POST['inputNom']) && !empty($_POST['inputPrenom'])) {
		//If post then record changes
		$controller->DBplannings->setPwdProfil(LOGIN, 
												trim_string($_POST['inputMdp']));
		$controller->DBplannings->setNomProfil(LOGIN, 
												trim_string($_POST['inputNom']));
		$controller->DBplannings->setPrenomProfil(LOGIN, 
												trim_string($_POST['inputPrenom']));
	}
	$dataProfil = $controller->DBplannings->getDataProfil(LOGIN);

	$data = array('myLogin' => LOGIN,
					'dataPwd' => $dataProfil['pwd'],
					'dataNom' => $dataProfil['nom'],
					'dataPrenom' => $dataProfil['prenom'],
					'dataStatut' => $dataProfil['statut'],
					'dataStatutaire' => $dataProfil['statutaire'],
					'dataActif' => $dataProfil['actif'],
					'dataAdministrateur' => stripcslashes($dataProfil['administrateur']));

	$controller->set($data);

	$controller->setLayout('user');

	$controller->render('profil', 'ProfilPerso', 'profil');
?>